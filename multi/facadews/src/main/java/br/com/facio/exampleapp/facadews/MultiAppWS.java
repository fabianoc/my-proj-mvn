/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.facadews;

import br.com.facio.exampleapp.customer.CustomerService;
import br.com.facio.exampleapp.product.ProductService;
import br.com.facio.exampleapp.repository.model.Customer;
import br.com.facio.exampleapp.repository.model.Product;
import java.util.Set;

/**
 *
 * @author fabiano
 */
public class MultiAppWS {
    
    private CustomerService customer = new CustomerService();
    private ProductService product = new ProductService();
    
    public String findAllCustomers() {
        Set<Customer> customers = customer.findAllCustomers();
        return customers.toString();
    }
    
    public String findAllProducts() {
        Set<Product> products = product.findAllProducts();
        return products.toString();
    }
}
