/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.product;

import br.com.facio.exampleapp.multi.utils.log.LogMulti;
import br.com.facio.exampleapp.repository.model.Product;
import br.com.facio.exampleapp.repository.repository.ProductMemoryRepository;
import java.util.Set;

/**
 *
 * @author fabiano
 */
public class ProductService {
    private static LogMulti LOG = LogMulti.getInstance();
    private ProductMemoryRepository productRepo = new ProductMemoryRepository();            

    public Set<Product> findAllProducts() {
        return productRepo.findAll();
    }
}
