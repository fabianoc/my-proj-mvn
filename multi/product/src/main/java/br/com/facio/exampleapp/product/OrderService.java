/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.product;

import br.com.facio.exampleapp.genericservices.notification.Notifier;
import br.com.facio.exampleapp.multi.utils.log.LogMulti;
import br.com.facio.exampleapp.repository.model.Customer;
import br.com.facio.exampleapp.repository.model.Order;
import br.com.facio.exampleapp.repository.model.Product;
import br.com.facio.exampleapp.repository.repository.OrderMemoryRepository;
import java.util.List;
import java.util.Random;

/**
 *
 * @author fabiano
 */
public class OrderService {
    
    private static LogMulti LOG = LogMulti.getInstance();
    private OrderMemoryRepository orderRepo = new OrderMemoryRepository();
    private Notifier notify = null;
    
    public void order(List<Product> items, Customer c) {
        LOG.debug("generating a order");
        
        Order o = new Order();
        
        o.setId(generateOrderId());
        o.setItems(items);
        o.setOwner(c);

        orderRepo.save(o);
        
        notify.byPhone(c, "Your order " + o.getId() + " is OK! Thanks for buy");
    }

    private Integer generateOrderId() {
        Random r = new Random();        
        return r.nextInt();
    }
}
