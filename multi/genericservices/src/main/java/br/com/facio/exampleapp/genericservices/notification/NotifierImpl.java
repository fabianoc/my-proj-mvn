/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.genericservices.notification;

import br.com.facio.exampleapp.genericservices.sms.SMSPlataform;
import br.com.facio.exampleapp.repository.model.Customer;

/**
 *
 * @author fabcipriano
 */
public class NotifierImpl implements Notifier{

    @Override
    public void byPhone(Customer c, String message) {
        SMSPlataform plataform = new SMSPlataform();
        
        plataform.send(c, message);
    }
    
}
