/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.genericservices.sms;

import br.com.facio.exampleapp.multi.utils.exception.MultiAppException;
import br.com.facio.exampleapp.multi.utils.log.LogMulti;
import br.com.facio.exampleapp.repository.model.Customer;

/**
 *
 * @author fabcipriano
 */
public class SMSPlataform {
    private static LogMulti LOG = LogMulti.getInstance();
    
    public void send(Customer c, String message) {
        LOG.debug("sending SMS to number " + c.getPhoneNumber() 
                + ", message=" + message);
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            throw new MultiAppException("Failed to send SMS");
        }
    }
}
