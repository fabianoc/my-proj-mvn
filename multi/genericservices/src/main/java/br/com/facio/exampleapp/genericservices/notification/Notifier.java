/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.genericservices.notification;

import br.com.facio.exampleapp.repository.model.Customer;

/**
 *
 * @author fabcipriano
 */
public interface Notifier {
    void byPhone(Customer c, String message);
}
