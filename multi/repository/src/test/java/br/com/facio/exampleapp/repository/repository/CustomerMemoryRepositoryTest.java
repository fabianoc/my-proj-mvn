/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.repository.repository;

import br.com.facio.exampleapp.multi.utils.exception.MultiAppException;
import br.com.facio.exampleapp.repository.model.Customer;
import br.com.facio.exampleapp.repository.model.CustomerBuilder;
import java.util.Set;
import junit.framework.TestCase;

/**
 *
 * @author fabiano
 */
public class CustomerMemoryRepositoryTest extends TestCase {
    
    public CustomerMemoryRepositoryTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testRepository() {
        CustomerBuilder builder = new CustomerBuilder();
        
        CustomerMemoryRepository repo = new CustomerMemoryRepository();

        Set<Customer> all = repo.findAll();
        
        assertEquals(0, all.size());
        
        Customer c1 = builder.setId(666)
                .setFirstName("Fabiano")
                .setMiddleName("Cipriano")
                .setLastName("de Oliveira")
                .setDocumentid("02957535602")
                .setPhoneNumber("996632986")
                .setEmail("fabcipriano@yahoo.com.br")
                .createCustomer();    
        
        
        try {
            repo.save(null);
            fail("this should launch a exception");
        } catch (MultiAppException mae) {
            /*exception ok*/
        } catch(Exception e) {
            fail("unexpected exception");
        }
        
        repo.save(c1);
        
        all = repo.findAll();
        assertEquals(1, all.size());
        
        Customer customer = repo.findById(666);
        
        assertEquals("Fabiano", customer.getFirstName());
        
        assertEquals(c1, customer);
    }
    
}
