/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.repository.repository;

import br.com.facio.exampleapp.multi.utils.exception.MultiAppException;
import br.com.facio.exampleapp.repository.model.Customer;
import br.com.facio.exampleapp.repository.model.Parameter;
import java.util.Set;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import junit.framework.TestCase;

/**
 *
 * @author fabiano
 */
public class ParameterMemoryRepositoryTest extends TestCase {
    
    public ParameterMemoryRepositoryTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testRepository() {
        ParameterMemoryRepository repo = new ParameterMemoryRepository();
        
        Parameter p1 = new Parameter(999, "MY_KEY", "Test 1 2 3");
        Parameter p2 = new Parameter(1000, "NEW_KEY", "Other");
        Parameter p3 = new Parameter(1001, "NEWEST_KEY", "Another Value");
        
        Set<Parameter> all = repo.findAll();
        
        assertEquals(0, all.size());
                
        
        try {
            repo.save(null);
            fail("this should launch a exception");
        } catch (MultiAppException mae) {
            /*exception ok*/
        } catch(Exception e) {
            fail("unexpected exception");
        }
        
        repo.save(p1);
        repo.save(p2);
        repo.save(p3);
        
        try {
            
            // duplicate exception
            repo.save(p3);
            fail("Must launch exception for duplicate exception");
        } catch (MultiAppException mae) {
            /*exception ok*/
        } catch(Exception e) {
            fail("unexpected exception");
        }
        
        all = repo.findAll();
        assertEquals(3, all.size());
        
        Parameter parameter = repo.findById(1001);
                
        assertEquals(p3, parameter);
                
        try {            
            repo.findById(123);
            fail("Must launch exception for no item with id 123");
        } catch (MultiAppException mae) {
            /*exception ok*/
        } catch(Exception e) {
            fail("unexpected exception");
        }
    }
    
}
