/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.repository;

import java.util.Comparator;

/**
 *
 * @author fabiano
 */
public abstract class PKComparableImpl implements PrimaryKey, Comparable<PrimaryKey> {

    public abstract Integer getId();

    public int compareTo(PrimaryKey o) {
        return getId().compareTo(o.getId());
    }

}
