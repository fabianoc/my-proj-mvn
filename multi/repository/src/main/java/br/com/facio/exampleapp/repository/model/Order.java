/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.repository.model;

import br.com.facio.exampleapp.repository.PKComparableImpl;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author fabiano
 */
public class Order extends PKComparableImpl {
    
    private Integer id;
    private List<Product> items;
    private Customer owner;
    private BigDecimal value;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the items
     */
    public List<Product> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(List<Product> items) {
        this.items = items;
    }

    /**
     * @return the owner
     */
    public Customer getOwner() {
        return owner;
    }

    /**
     * @param owner the owner to set
     */
    public void setOwner(Customer owner) {
        this.owner = owner;
    }

    /**
     * @return the value
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", items=" + items + ", owner=" + owner + ", value=" + value + '}';
    }
    
    
}
