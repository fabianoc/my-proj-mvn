/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.repository.repository;

import br.com.facio.exampleapp.repository.PrimaryKey;
import java.util.Set;

/**
 *
 * @author fabcipriano
 */
public interface GenericRepository <T extends PrimaryKey> {

    Set<T> findAll();

    T findById(int id);

    void save(T c);
    
}
