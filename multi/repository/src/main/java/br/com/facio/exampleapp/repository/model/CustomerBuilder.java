/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.repository.model;


public class CustomerBuilder {
    private int id;
    private String firstName;
    private String middleName = null;
    private String lastName;
    private String documentid = null;
    private String phoneNumber = null;
    private String email = null;

    public CustomerBuilder() {
    }

    public CustomerBuilder setId(int id) {
        this.id = id;
        return this;
    }

    public CustomerBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public CustomerBuilder setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public CustomerBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public CustomerBuilder setDocumentid(String documentid) {
        this.documentid = documentid;
        return this;
    }

    public CustomerBuilder setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public CustomerBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public Customer createCustomer() {
        return new Customer(id, firstName, middleName, lastName, documentid, phoneNumber, email);
    }
    
}
