/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.repository.repository;

import br.com.facio.exampleapp.multi.utils.exception.MultiAppException;
import br.com.facio.exampleapp.multi.utils.log.LogMulti;
import br.com.facio.exampleapp.repository.PrimaryKey;
import br.com.facio.exampleapp.repository.model.Customer;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author fabiano
 * @param <T>
 */
public class GenericMemoryRepository<T extends PrimaryKey> implements GenericRepository<T> {
    
    private static LogMulti LOG = LogMulti.getInstance();
    
    private final Set<T> storeditems = new TreeSet();
    

    public Set<T> findAll() {
        LOG.debug("findAll");
        return Collections.unmodifiableSet(storeditems);        
    }

    public T findById(int id) {
        T result = null;
        
        LOG.debug("findById(" + id + ")");
        for (T t : storeditems) {
            if (t.getId() == id) {
                result = t;
                break;
            }
        }
        
        /* Code Clean never return null to prevent null checks*/
        if (result == null) {
            throw new MultiAppException("Failed to find stored item by Id");
        }
        
        return result;
    }

    public void save(T c) {
        if (c == null) {
            throw new MultiAppException("Stored item cannot be null");
        }
        
        LOG.debug("save(" + c + ")");
        if (!storeditems.add(c)) {
            LOG.debug("item with=" + c.getId() + " ALREADY EXIST!!!");
            throw new MultiAppException("Constraint id exception... Sotered item already exist ");
        }
    }
    
}
