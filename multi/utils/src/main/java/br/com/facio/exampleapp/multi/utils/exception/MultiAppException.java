/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.multi.utils.exception;

/**
 *
 * @author fabcipriano
 */
public class MultiAppException extends RuntimeException {

    public MultiAppException() {
    }

    public MultiAppException(String message) {
        super(message);
    }

    public MultiAppException(String message, Throwable cause) {
        super(message, cause);
    }

    public MultiAppException(Throwable cause) {
        super(cause);
    }

    public MultiAppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
        
}
