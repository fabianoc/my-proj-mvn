/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.multi.utils.log;

/**
 *
 * @author fabcipriano
 */
public class LogMulti {
    
    private static LogMulti INSTANCE = null;
    
    private LogMulti() {
    }
    
    public static LogMulti getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new LogMulti();
        }
        
        return INSTANCE;
    }
    
    public static void debug(String message) {
        printlog(message, null);
    }

    public static void debug(String message, Throwable t) {
        printlog(message, t);
    }
    
    private static void printlog(String message, Throwable t) {
        System.out.println(message);
        if (t != null) {
            t.printStackTrace();
        }
    }
}
