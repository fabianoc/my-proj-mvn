/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.facio.exampleapp.customer;

import br.com.facio.exampleapp.genericservices.notification.Notifier;
import br.com.facio.exampleapp.multi.utils.log.LogMulti;
import br.com.facio.exampleapp.repository.model.Customer;
import br.com.facio.exampleapp.repository.repository.CustomerMemoryRepository;
import java.util.Set;

/**
 *
 * @author fabiano
 */
public class CustomerService {

    private static LogMulti LOG = LogMulti.getInstance();
    private CustomerMemoryRepository customerRepo = new CustomerMemoryRepository();
    private Notifier notify = null;
    
    public void createCustomer(Customer c) {
        validateAndCompleteCustomerInformation(c);
        
        customerRepo.save(c);
        customerRepo.save(c);
    }
    
    public Set<Customer> findAllCustomers() {
        return customerRepo.findAll();
    }

    private void validateAndCompleteCustomerInformation(Customer c) {
        verifyLegalInformation(c);
        completeCustomerInformation(c);
        
        notify.byPhone(c, "Thanks and Congratiolations we are ower client now!!!");
    }

    private void verifyLegalInformation(Customer c) {
        LOG.debug("verifying if client is NOT Lava Jato");
    }

    private void completeCustomerInformation(Customer c) {
        LOG.debug("get infation from Facebook and Google");
    }
    
}
